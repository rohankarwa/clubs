from sys import argv

script, input_file, output_file = argv

output_file_holder = open(output_file, "w")
input_file_holder = open(input_file, "r")

id = 1
for line in input_file_holder:
	output_file_holder.write(str(id) + "," + line)
	id = id + 1

output_file_holder.close()
input_file_holder.close()	

