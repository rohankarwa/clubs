\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}Adaptive CLUBS Approach}{3}
\contentsline {section}{\numberline {2.1}Background}{3}
\contentsline {section}{\numberline {2.2}Promising Dimension Prediction Approach}{4}
\contentsline {section}{\numberline {2.3}Key challenges associated with the approach}{6}
\contentsline {subsection}{\numberline {2.3.1}Choosing the Sample Size: Trade off with performance and accuracy}{6}
\contentsline {subsection}{\numberline {2.3.2}Making the Sample Size Adaptive and Curve Tracking}{7}
\contentsline {chapter}{\numberline {3}Implementation}{9}
\contentsline {chapter}{\numberline {4}Datasets}{10}
\contentsline {chapter}{\numberline {5}Experiment Details}{12}
\contentsline {section}{\numberline {5.1}Deciding upon Initial\_Sample\_Size}{12}
\contentsline {section}{\numberline {5.2}Cluster Quality Measurement}{13}
\contentsline {section}{\numberline {5.3}Performing KMeans on the obtained Clusters}{13}
\contentsline {chapter}{\numberline {6}Results and Evaluation}{15}
\contentsline {section}{\numberline {6.1}Results for Finding Initial\_Sample\_Size }{15}
\contentsline {subsection}{\numberline {6.1.1}Fixed Sample Start Size: 40, irrespective of the cluster size}{16}
\contentsline {subsection}{\numberline {6.1.2}Fixed Sample Start Size: 200, irrespective of the cluster size}{17}
\contentsline {subsection}{\numberline {6.1.3}Fixed Sample Start Size: 1000, irrespective of the cluster size}{18}
\contentsline {subsection}{\numberline {6.1.4}Sample Size: 1\% of the cluster size}{19}
\contentsline {subsection}{\numberline {6.1.5}Sample Size: 2\% of the cluster size}{20}
\contentsline {subsection}{\numberline {6.1.6}Sample Size: 5\% of the cluster size}{21}
\contentsline {subsection}{\numberline {6.1.7}Sample Size: 10\% of the cluster size}{22}
\contentsline {section}{\numberline {6.2}New heuristics: Sample Size = Max (1\% of the cluster size, 40)}{25}
\contentsline {section}{\numberline {6.3}KMeans on the Generated Clusters}{28}
\contentsline {chapter}{\numberline {7}Discussion}{31}
\contentsline {chapter}{\numberline {8}Conclusion}{34}
\contentsline {chapter}{\numberline {9}Acknowledgments}{35}
\contentsline {chapter}{References}{36}
