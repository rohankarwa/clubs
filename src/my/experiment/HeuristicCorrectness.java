package my.experiment;

import java.util.ArrayList;
import java.util.List;

import org.clustering.clubs.Cluster;
import org.clustering.clubs.DataPoint;
import org.clustering.clubs.DimensionMetadata;
import org.clustering.clubs.SSQ;

public class HeuristicCorrectness {

	public void doExperiment() {
		Cluster cluster = createCluster();
		System.out.println("Cluster SSQ: " + cluster.calculateClusterSSQ());
		DimensionMetadata dimensionMetadata0 = SSQ.calculateDimensionMetadaNew(cluster, 0);
		System.out.println(dimensionMetadata0);
		
		DimensionMetadata dimensionMetadata1 = SSQ.calculateDimensionMetadaNew(cluster, 1);
		System.out.println(dimensionMetadata1);
		
	}

	private Cluster createCluster() {
		Cluster cluster = new Cluster(2, "1");
		List<DataPoint> list = new ArrayList<DataPoint>();
		list.add(new DataPoint("1", new double[]{1,1}));
		list.add(new DataPoint("2", new double[]{1,2}));
		
		
		list.add(new DataPoint("3", new double[]{2,1}));
		list.add(new DataPoint("4", new double[]{2,2}));
	
	/*	
		list.add(new DataPoint("5", new double[]{3,1}));
		list.add(new DataPoint("6", new double[]{3,2}));
	*/	
	//	list.add(new DataPoint("7", new double[]{4,0.5}));
		
		/*
		list.add(new DataPoint("8", new double[]{5,8}));
		list.add(new DataPoint("9", new double[]{6,9}));
		list.add(new DataPoint("10", new double[]{7,10}));
		*/
		
		cluster.addDataPoints(list);
		return cluster;
	}
	
	public static void main(String[] args) {
		new HeuristicCorrectness().doExperiment();
	}
}
