package my.tree;
/*
public class MyTreeNormal implements MyTree{

	private Node head;
	
	@Override
	public void insertValue(double value) {
		head = insertValue(head, value);		
	}

	@Override
	public double getCummulativeSum(double key) {
		Node node = getNodeWithKey(head, key);
		return node.sum;
	}
	
	public void print() {
		print(head);
	}
	
	private void print(Node node) {
		if(node == null) return;
		print(node.left);
		System.out.println(node.key + ":" + node.sum );
		print(node.right);
	}
	
	private Node getNodeWithKey(Node node, double key) {
		if(node == null || node.key == key) {
			return null;
		}
		if(node.key > key) return getNodeWithKey(node.left, key);
		else return getNodeWithKey(node.right, key);
	}

	private Node insertValue(Node node, double value) {
		if(node == null) {
			return new Node(value, value);
		}
		if(node.key == value) {
			node.sum += value;
			return node;
		}
		if(node.key > value) {
			Node leftNode = insertValue(node.left, value);
			node.sum = node.key + leftNode.sum;
			node.left = leftNode;
		} else {
			node.right = insertValue(node.right, value);
		}
		return node;
	}
	
	private class Node {
		Node left = null;
		Node right = null;
		double key;
		double sum = 0;
		public Node(double key, double sum) {
			this.key = key;
			this.sum = sum;
		}
	}
	
	public static void main(String[] args) {
		MyTreeNormal tree = new MyTreeNormal();
		tree.insertValue(1);
		tree.insertValue(3);
		tree.insertValue(2);
		
		tree.print();
	}

	@Override
	public void updateSum() {
		// TODO Auto-generated method stub
		
	}

}
*/