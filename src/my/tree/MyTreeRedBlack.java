package my.tree;

import java.util.Map;
import java.util.TreeMap;

public class MyTreeRedBlack implements MyTree{

	private TreeMap<Double, MyTreeValue> treeMap = new TreeMap<Double, MyTreeValue>();
	
	@Override
	public void insertValue(double key, double[] values) {
		values = values.clone();
		if(treeMap.containsKey(key)) {
			MyTreeValue existingValue = treeMap.get(key);
			double[] exisitingCoordinates = existingValue.getCoordinates();
			for(int i = 0; i < exisitingCoordinates.length; i++) {
				exisitingCoordinates[i] += values[i];
			}
			existingValue.setCoordinates(exisitingCoordinates);
			existingValue.setTotalPoints(existingValue.getTotalPoints()+1);
		} else {
			treeMap.put(key, new MyTreeValue(values, 1));
		}
	}
	
	@Override
	public void updateSum() {
		MyTreeValue lastValue = null;
		for(Map.Entry<Double, MyTreeValue> entry : treeMap.entrySet()) {
			MyTreeValue newValue = getNewValue(entry.getValue(), lastValue);
			entry.setValue(newValue);
			lastValue = newValue;
		}
	}
	

	@Override
	public MyTreeValue getMaxValue() {
		return treeMap.lastEntry().getValue();
	}

	private MyTreeValue getNewValue(MyTreeValue myTreeValue, MyTreeValue lastVal) {
		if(lastVal == null) return myTreeValue;
		
		double[] lastValue = lastVal.getCoordinates().clone();
		double[] value = myTreeValue.getCoordinates().clone();
		for(int i = 0; i< value.length; i++) {
			value[i] = value[i] + lastValue[i];
		}
		myTreeValue.setCoordinates(value);
		myTreeValue.setTotalPoints(myTreeValue.getTotalPoints() + lastVal.getTotalPoints());
		return myTreeValue;
	}

	@Override
	public MyTreeValue getCummulativeSum(double key) {
		return treeMap.get(key);	
	}
	
	public void print() {
		System.out.println(treeMap);
	}

	public static void main(String args[]) {
		MyTreeRedBlack myTreeRedBlack = new MyTreeRedBlack();
		myTreeRedBlack.insertValue(1, new double[] {1, 1});
		myTreeRedBlack.insertValue(3, new double[]{(double) 3,(double) 1});
		myTreeRedBlack.insertValue(2, new double[]{(double) 2,(double) 1});
		myTreeRedBlack.insertValue(2, new double[]{(double) 2,(double) 1});
		myTreeRedBlack.updateSum();
		
		myTreeRedBlack.print();
		
		Double[] a = new Double[2];
		a[0] = new Double(1);
		a[1] = new Double(2);
		
		//a[]
	}


}
