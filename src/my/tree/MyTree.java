package my.tree;

public interface MyTree {

	public void insertValue(double key, double[] value);
	
	public MyTreeValue getCummulativeSum(double key);
	
	public void updateSum();
	
	public MyTreeValue getMaxValue();
}
