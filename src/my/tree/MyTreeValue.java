package my.tree;

public class MyTreeValue {

	private double[] coordinates;
	private int totalPoints;
	public MyTreeValue(double[] coordinates, int totalPoints) {
		this.coordinates = coordinates;
		this.totalPoints = totalPoints;
	}
	
	public double[] getCoordinates() {
		return coordinates;
	}
	
	public int getTotalPoints() {
		return totalPoints;
	}
	
	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}
	
	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}
	
	@Override
	public String toString() {
		return "TotalPoints = " + totalPoints + ", Sum: " + stringValue(coordinates);
	}

	private String stringValue(double[] value) {
		StringBuilder builder = new StringBuilder();
		for(double val : value) {
			builder.append(", " + val);
		}
		return builder.toString();
	}
}
