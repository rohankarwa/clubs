package org.file;

public class MyFileException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFileException(String message) {
		super(message);
	}
}
