package org.clustering.clubs;

public class DataPoint {

	private double[] coordinates;
	private String id;
	
	public DataPoint(String id, double[] coordinates) {
		//Clone the coordinates to avoid unintentional change to datapoint.
		//Making it a thread safe.
		this.coordinates = clone(coordinates);
		this.id = id;
	}
	
	public double[] getCoordinates() {
		return clone(coordinates);
	}
	
	public String getId() {
		return id;
	}
	
	private double[] clone(double[] array) {
		double[] cloneCopy = new double[array.length];
		for(int i = 0; i < array.length; i++) {
			cloneCopy[i] = array[i];
		}
		return cloneCopy;
	}
	
	@Override
	public String toString() {
		return "ID: " + id;// + ", Coordinates: " + getCoordinatesInString();
	}

	private String getCoordinatesInString() {
		StringBuilder builder = new StringBuilder();
		for(double coordinate : coordinates) {
			builder.append(", " + coordinate);
		}
		return builder.substring(1);
	}
}
