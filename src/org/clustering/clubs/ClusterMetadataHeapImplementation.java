package org.clustering.clubs;

import org.heap.MyHeap;

public class ClusterMetadataHeapImplementation implements ClusterMetadata{

	//TODO: Change default value 100
	private MyHeap<DimensionMetadata, Double> maxHeap = 
									new MyHeap<DimensionMetadata, Double>(1000, false);
	
	public DimensionMetadata getDimensionWithMaxSSQ() {
		if(maxHeap.size() == 0) {
			return null;
		}
		return maxHeap.peek();
	}
	
	public DimensionMetadata removeDimensionWithMaxSSQ() {
		if(maxHeap.size() == 0) {
			return null;
		}
		return maxHeap.removeElement();
	}
	
	public void addDimensionMetadata(DimensionMetadata dimensionMetadata) {
		maxHeap.addToHeap(dimensionMetadata, dimensionMetadata.getMaxDeltaSSQValue());
	}

	public ClusterMetadata getClusterMetadataCopyForSplittedCluster() {
		ClusterMetadata clone = new ClusterMetadataHeapImplementation();
		MyHeap<DimensionMetadata, Double> currentHeap = new MyHeap<DimensionMetadata, Double>(1000, false);
		
		DimensionMetadata dimensionMetada = removeDimensionWithMaxSSQ();
		while(dimensionMetada != null) {
			DimensionMetadata dimensionMetadaNew = new DimensionMetadata(dimensionMetada.getDimension(), 
														true, dimensionMetada.getMaxDeltaSSQValue(),
														dimensionMetada.getSplitPosition());
			clone.addDimensionMetadata(dimensionMetadaNew);
			currentHeap.addToHeap(dimensionMetada, dimensionMetada.getMaxDeltaSSQValue());
			dimensionMetada = removeDimensionWithMaxSSQ();
		}
		maxHeap = currentHeap;
		return clone;
	}

	@Override
	public DimensionMetadata getDimensionMetadata(int dimension) {
		// Not supported
		return null;
	}

}
