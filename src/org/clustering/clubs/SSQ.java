package org.clustering.clubs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.clustering.clubs.logger.Logger;

public class SSQ {

	public static double calculateSSQ(Cluster cluster) {
		double ssq = 0;
		for(int dimension = 0; dimension < cluster.getTotalDimensions(); dimension++) {
			ssq += (cluster.getSquareSumForDimension(dimension) 
							 - (Math.pow(cluster.getLinearForDimension(dimension), 2)/cluster.getTotalDataPoints()));
		}
		return ssq;
	}
	
	/*Not Used
	 * 
	 */
	/*
	public static double calculateSSQ(double[] linearSum, double[] squareSum, double totalPoints) {
		double ssq = 0;
		int totalDimensions = linearSum.length;
		for(int dimension = 0; dimension < totalDimensions; dimension++) {
			ssq += (squareSum[dimension] 
							 - (Math.pow(linearSum[dimension], 2)/totalPoints));
		}
		return ssq;
	}*/
	
	public static double calculateSSQReduction(double[] linearSum1, int N1, double[] linearSum2, double N2) {
		double reduction = 0;
		for(int dimension = 0; dimension < linearSum1.length; dimension++) {
			double lhs = linearSum1[dimension]/N1;
			double rhs = linearSum2[dimension]/N2;
			reduction += Math.pow(lhs - rhs, 2);
		}
		
		double finalReduction = ((N1 * N2)/(N1 + N2) ) * reduction;
		return finalReduction;
	}
	
	public static double calculateDeltaSSQ(Cluster cluster, int dimension, 
												double splitLocation) {
		Cluster clusterLeft = new Cluster(cluster.getTotalDimensions(), cluster.getClusterId() + "dump1");
		Cluster clusterRight = new Cluster(cluster.getTotalDimensions(), cluster.getClusterId() + "dump2");
		
		for(DataPoint dataPoint : cluster.getDataPoints()) {
			if(dataPoint.getCoordinates()[dimension] <= splitLocation) {
				clusterLeft.addDataPoint(dataPoint);
			} else {
				clusterRight.addDataPoint(dataPoint);
			}
		}
		
		if(clusterLeft.getTotalDataPoints() == 0 ||
				clusterRight.getTotalDataPoints() == 0) {
			return 0;
		}
		
		//double deltaSSQ = calculateSSQ(cluster) - calculateSSQ(clusterLeft) - calculateSSQ(clusterRight);
		double deltaSSQNew = calculateSSQReduction(clusterLeft.getLinearSum(), clusterLeft.getTotalDataPoints(), 
													clusterRight.getLinearSum(), clusterRight.getTotalDataPoints());
		
		return deltaSSQNew;
	}
	
	public static double calculateSSQGainIfMerged(Cluster cluster1, Cluster cluster2) {
		Cluster mergedCluster = new Cluster(cluster1.getTotalDimensions(), "dumpMergedCluster");
		mergedCluster.addDataPoints(cluster1.getDataPoints());
		mergedCluster.addDataPoints(cluster2.getDataPoints());
		
		double deltaSSQ = calculateSSQ(mergedCluster) 
								- calculateSSQ(cluster1) - calculateSSQ(cluster2);
		return deltaSSQ;
	}
	
	
	/*
	 * Use New method
	 */
	/*
	public static DimensionMetadata calculateDimensionMetada(Cluster cluster, int dimension) {
		Logger.logMessage("Calculating DimensionMetadata for Dimension: "  + dimension + ", Cluster Data Point: " + cluster.getTotalDataPoints());
		double ssqForCluster = calculateSSQ(cluster);
		Map<Double, List<DataPoint>> dataPointMap = new HashMap<Double, List<DataPoint>>();
		Set<Double> points = new TreeSet<Double>();
		for(DataPoint dataPoint : cluster.getDataPoints()) {
			double cordinateValue = dataPoint.getCoordinates()[dimension];
			if(!dataPointMap.containsKey(cordinateValue)) {
				dataPointMap.put(cordinateValue, new ArrayList<DataPoint>());
				points.add(cordinateValue);
			}
			dataPointMap.get(cordinateValue).add(dataPoint);
		}
		
		double[] linearSumForRightCluster = cluster.getLinearSum();
		double[] squareSumForRightCluster = cluster.getSquareSum();
		int totalPointInRightCluster = cluster.getTotalDataPoints();
		
		double[] linearSumForLeftCluster = new double[cluster.getTotalDimensions()];
		double[] squareSumForLeftCluster = new double[cluster.getTotalDimensions()];
		int totalPointInLeftCluster = 0;
		
		double maxGain = 0;
		double finalSplitPosition = -1;
		for(Double splitPosition : points) {
			for(DataPoint dataPoint : dataPointMap.get(splitPosition)) {
				updateRightClusterInvariants(linearSumForRightCluster, 
											 squareSumForRightCluster,
											 dataPoint);
				updateLeftClusterInvariants(linearSumForLeftCluster,
											squareSumForLeftCluster,
											dataPoint);
				totalPointInLeftCluster++;
				totalPointInRightCluster--;
			}
			
			if(totalPointInLeftCluster <= 0 || totalPointInRightCluster <=0) {
				continue;
			}
			double gain = ssqForCluster - calculateSSQ(linearSumForLeftCluster, squareSumForLeftCluster, totalPointInLeftCluster)
					       			    - calculateSSQ(linearSumForRightCluster, squareSumForRightCluster, totalPointInRightCluster);
			if(gain > maxGain) {
				maxGain = gain;
				finalSplitPosition = splitPosition;
			}
		}
		return new DimensionMetadata(dimension, false, maxGain, finalSplitPosition);
	}
	*/
	
	public static DimensionMetadata calculateDimensionMetadaNew(Cluster cluster, int dimension) {
		Logger.logMessage("Calculating DimensionMetadata for Dimension: "  + dimension + ", Cluster Data Point: " + cluster.getTotalDataPoints());
		Map<Double, List<DataPoint>> dataPointMap = new HashMap<Double, List<DataPoint>>();
		Set<Double> points = new TreeSet<Double>();
		for(DataPoint dataPoint : cluster.getDataPoints()) {
			double cordinateValue = dataPoint.getCoordinates()[dimension];
			if(!dataPointMap.containsKey(cordinateValue)) {
				dataPointMap.put(cordinateValue, new ArrayList<DataPoint>());
				points.add(cordinateValue);
			}
			dataPointMap.get(cordinateValue).add(dataPoint);
		}
		
		double[] linearSumForRightCluster = cluster.getLinearSum();
		double[] squareSumForRightCluster = cluster.getSquareSum();
		int totalPointInRightCluster = cluster.getTotalDataPoints();
		
		double[] linearSumForLeftCluster = new double[cluster.getTotalDimensions()];
		double[] squareSumForLeftCluster = new double[cluster.getTotalDimensions()];
		int totalPointInLeftCluster = 0;
		
		double maxReduction = 0;
		double finalSplitPosition = -1;
		for(Double splitPosition : points) {
			for(DataPoint dataPoint : dataPointMap.get(splitPosition)) {
				updateRightClusterInvariants(linearSumForRightCluster, 
											 squareSumForRightCluster,
											 dataPoint);
				updateLeftClusterInvariants(linearSumForLeftCluster,
											squareSumForLeftCluster,
											dataPoint);
				totalPointInLeftCluster++;
				totalPointInRightCluster--;
			}
			
			if(totalPointInLeftCluster <= 0 || totalPointInRightCluster <=0) {
				continue;
			}
			double reduction = calculateSSQReduction(linearSumForLeftCluster, totalPointInLeftCluster, 
													linearSumForRightCluster, totalPointInRightCluster);
			
			//System.out.println("Reduction: " + reduction + ", split Position= " + splitPosition);
			if(reduction > maxReduction) {
				maxReduction = reduction;
				finalSplitPosition = splitPosition;
			}
		}
		return new DimensionMetadata(dimension, false, maxReduction, finalSplitPosition);
	}

	private static void updateRightClusterInvariants(
			double[] linearSumForRightCluster,
			double[] squareSumForRightCluster, DataPoint dataPoint) {
		double[] coordinates = dataPoint.getCoordinates();
		for(int dimension = 0; dimension < coordinates.length; dimension++) {
			linearSumForRightCluster[dimension] -= coordinates[dimension];
			squareSumForRightCluster[dimension] -= Math.pow(coordinates[dimension], 2);
		}
	}

	private static void updateLeftClusterInvariants(
			double[] linearSumForLeftCluster, double[] squareSumForLeftCluster,
			DataPoint dataPoint) {
		double[] coordinates = dataPoint.getCoordinates();
		for(int dimension = 0; dimension < coordinates.length; dimension++) {
			linearSumForLeftCluster[dimension] += coordinates[dimension];
			squareSumForLeftCluster[dimension] += Math.pow(coordinates[dimension], 2);
		}
	}
}
