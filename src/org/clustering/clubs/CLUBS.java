package org.clustering.clubs;

public interface CLUBS {

	public Cluster[] doClustering(String dataFile, String tokenizer, int idColumn);
}
