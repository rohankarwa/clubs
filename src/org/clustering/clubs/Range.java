package org.clustering.clubs;

import org.clustering.clubs.logger.Logger;

public class Range {
	
	private double lowerBound = Double.MIN_VALUE;
	private double upperBound = Double.MAX_VALUE;
	
	public Range(double lowerBound, double upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	public double getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}

	public double getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}

	public void sanity() {
		if(lowerBound > upperBound) {
			Logger.logMessage(lowerBound + ":"  + upperBound);
			Logger.logMessage("SANITY BROKE");
			System.exit(0);
		}
		
	}
}
