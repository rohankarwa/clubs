package org.clustering.clubs;

public class DimensionMetadata {

	private int dimension;
	private boolean isSSQValueEstimated;
	private double maxdeltaSSQValue;
	private double splitPosition;
	private static final int PLATEAU_PERCENT_LENGHT = 30;
	
	//Keep a note of max SSQ Value as per the data point progress.. 5%, 10%,...
	private double[] maxSSQValueDataProgress = new double[20]; 
	
	public DimensionMetadata(int dimension, boolean isSSQValueEstimated,
			double maxSSQValue, double splitPosition) {
		this.dimension = dimension;
		this.isSSQValueEstimated = isSSQValueEstimated;
		this.maxdeltaSSQValue = maxSSQValue;
		this.splitPosition = splitPosition;
	}

	public int getDimension() {
		return dimension;
	}

	public boolean isSSQValueEstimated() {
		return isSSQValueEstimated;
	}

	public double getMaxDeltaSSQValue() {
		return maxdeltaSSQValue;
	}
	
	public double getSplitPosition() {
		return splitPosition;
	}
	
	@Override
	public String toString() {
		return "DimensionMetatadata(Dimension: " + dimension + ", isSSQFinal: " + !isSSQValueEstimated 
						+ ", maxDeltaSSQ: " + maxdeltaSSQValue + ", splitPosition: " + splitPosition + ")";
	}
	
	public void updateMaxSSQForPercent(double ssqValue, int percent, double splitLocation) {
		int arrayPosition = percent/5 - 1;
		if(arrayPosition != 0) {
			if(maxSSQValueDataProgress[arrayPosition - 1] == 0) {
				refillZeroValues(maxSSQValueDataProgress, arrayPosition -1);
			}
			maxSSQValueDataProgress[arrayPosition] = Math.max(maxSSQValueDataProgress[arrayPosition],
															maxSSQValueDataProgress[arrayPosition-1]);
		}
		
		if(ssqValue > maxSSQValueDataProgress[arrayPosition]) {
			this.splitPosition= splitLocation;
			this.maxdeltaSSQValue = ssqValue;
			maxSSQValueDataProgress[arrayPosition] = ssqValue;	
		}
	}

	private void refillZeroValues(double[] array, int position) {
		for(int i = 1; i<= position; i++) {
			array[i] = Math.max(array[i-1], array[i]);
		}
		
	}	
	
	public double[] getMaxSSQValueDataProgress() {
		return maxSSQValueDataProgress;
	}
	
	/*
	private boolean isPlateauReached(double epsilon) {
		int lowerPercentLimit = 70;
		int lowerIndex = lowerPercentLimit/5 - 1;
		double percentValue100 = maxSSQValueDataProgress[maxSSQValueDataProgress.length - 1];
		double percentValue70 = maxSSQValueDataProgress[lowerIndex];
		double error = (percentValue100 - percentValue70)/percentValue70;
		if(error < epsilon) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isDimensionPlateauReached(double epsilon) {
		int arrayUpperBound = getMaxIndexWithNonZeroValue();
		int percentBound = (arrayUpperBound + 1) * 5;
		if(isPlateauReachedTillSpecificPercent(percentBound, epsilon)) 
			return true;
		else
			return false;
	}
	*/
	
	public void extrapolateMaxSSQValueTill100Percent() {
		int maxFilledIndex = getMaxIndexWithNonZeroValue();
		double maxValue = maxSSQValueDataProgress[maxFilledIndex];
		for(int position = maxFilledIndex + 1; position < maxSSQValueDataProgress.length; position++) {
			maxSSQValueDataProgress[position] = maxValue;
		}
	}
	
	private int getMaxIndexWithNonZeroValue() {
		int position = maxSSQValueDataProgress.length - 1;
		while(maxSSQValueDataProgress[position] == 0 && position >= 0) {
			position--;
		}
		if(position < 0) {
			System.out.println("Something is wrong");
		}
		return position;
	}
	

	
	public boolean isPlateauReachedTillSpecificPercent(int percentBound, double epsilon) {
		int arrayPosition = percentBound/5 - 1;
		int lowerBound = arrayPosition - (PLATEAU_PERCENT_LENGHT/5 - 1);
		
		if(lowerBound < 0) return false;
		
		if(maxSSQValueDataProgress[arrayPosition] == 0) {
			maxSSQValueDataProgress[arrayPosition] = maxSSQValueDataProgress[arrayPosition - 1];
		}
		
		double error = ((maxSSQValueDataProgress[arrayPosition] - maxSSQValueDataProgress[lowerBound])/maxSSQValueDataProgress[lowerBound]); 
		if(error < epsilon) {
			return true;
		} else {
			return false;
		}
	}
}
