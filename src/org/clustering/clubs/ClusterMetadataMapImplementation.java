package org.clustering.clubs;

import java.util.HashMap;
import java.util.Map;

public class ClusterMetadataMapImplementation implements ClusterMetadata {

	// TODO: Change default value 100
	private Map<Integer, DimensionMetadata> map = new HashMap<Integer, DimensionMetadata>();

	public DimensionMetadata getDimensionWithMaxSSQ() {
		//Not supported
		return null;
	}

	public DimensionMetadata removeDimensionWithMaxSSQ() {
		//Not supported
		return null;
	}

	public void addDimensionMetadata(DimensionMetadata dimensionMetadata) {
		map.put(dimensionMetadata.getDimension(), dimensionMetadata);
	}

	public ClusterMetadata getClusterMetadataCopyForSplittedCluster() {
		ClusterMetadata clone = new ClusterMetadataMapImplementation();
		for(int dimension : map.keySet()) {
			DimensionMetadata dimensionMetada = map.get(dimension);
			DimensionMetadata dimensionMetadaNew = new DimensionMetadata(
					dimensionMetada.getDimension(), true,
					dimensionMetada.getMaxDeltaSSQValue(),
					dimensionMetada.getSplitPosition());
			clone.addDimensionMetadata(dimensionMetadaNew);
		}
		return clone;
	}

	@Override
	public DimensionMetadata getDimensionMetadata(int dimension) {
		return map.get(dimension);
	}

}
