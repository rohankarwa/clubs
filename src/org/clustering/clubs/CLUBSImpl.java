package org.clustering.clubs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import my.tree.MyTree;
import my.tree.MyTreeRedBlack;
import my.tree.MyTreeValue;

import org.clustering.clubs.input.InputDataManager;
import org.clustering.clubs.logger.Logger;
import org.clustering.clubs.logger.SpitGraph;
import org.clustering.clubs.logger.TimeMeasurement;
import org.heap.MyHeap;

public class CLUBSImpl implements CLUBS {

	private InputDataManager inputDataManger;
	private static final double P = 0.8;
	private CLUBSApproach clubsApproach;
	
	public CLUBSImpl(InputDataManager inputDataManager, CLUBSApproach clubsApproach) {
		this.inputDataManger = inputDataManager;
		this.clubsApproach = clubsApproach;
	}
	
	@Override
	public Cluster[] doClustering(String dataFile, String tokenizer, int idColumn) {
		TimeMeasurement.logTime("STANDARD CLUBS: Measurements for file: " + dataFile);
		StringBuilder builder = new StringBuilder();
		
		long startTime = System.currentTimeMillis();
		System.out.println("Start Time: " + startTime);
		
		Cluster cluster = inputDataManger.loadFileDataAsSingleCluster(dataFile, tokenizer, idColumn);
		
		if(clubsApproach.equals(CLUBSApproach.CLUBS_HEURISITICS_UPPER_BOUND)) {
			cluster.setClusterMetadata(new ClusterMetadataHeapImplementation());
		} else {
			cluster.setClusterMetadata(new ClusterMetadataMapImplementation());
		}

		//cluster.configureClusterMetadata();
		
		
		double ssq0 = cluster.calculateClusterSSQ();
		double meanSSQ0 = ssq0/cluster.getTotalDataPoints();
		
		long initializeTime = System.currentTimeMillis();
		System.out.println("Intialize Time = " + (initializeTime - startTime));
		TimeMeasurement.logTime("Intialize Time = " + (initializeTime - startTime));
		builder.append((initializeTime - startTime) + "\t" + "NA");
		
		Cluster[] clusters = doTopDownSplitting(cluster, meanSSQ0);
		long topDowntime = System.currentTimeMillis();
		System.out.println("Top Down Time = " + (topDowntime - initializeTime));
		TimeMeasurement.logTime("Top Down Time = " + (topDowntime - initializeTime));
		builder.append("\t" + (topDowntime - initializeTime));
		
		clusters = doBottomUpMerging(clusters, meanSSQ0);
		long bottupUpTime = System.currentTimeMillis();
		System.out.println("Agglomerative Time = " + (bottupUpTime - topDowntime));
		TimeMeasurement.logTime("Agglomerative Time = " + (bottupUpTime - topDowntime));
		builder.append("\t" +  (bottupUpTime - topDowntime));
		
		long endTime = System.currentTimeMillis();
		System.out.println("Total Time = " + (endTime- startTime));
		TimeMeasurement.logTime("Total Time = " + (endTime - startTime));
		builder.append("\t" + (endTime - startTime));
		
		TimeMeasurement.logTime(builder.toString());
		return clusters;
	}

	private Cluster[] doTopDownSplitting(Cluster cluster, double meanSSQ0) {
		MyHeap<Cluster, Double> clusterHeap = new MyHeap<Cluster, Double>(100, false);
		double ssq0 = cluster.calculateClusterSSQ();
		clusterHeap.addToHeap(cluster, ssq0);
		int iteration = 0;
		while(true) {
			iteration++;
			Logger.logMessage("************START******************");
//			System.out.println("Print Heap: " + clusterHeap);
			Cluster clusterToSplit = clusterHeap.peek();
			
			DimensionMetadata dimensionToSplit = null;
			
			if(clubsApproach.equals(CLUBSApproach.CLUBS_HEURISITICS_UPPER_BOUND)) {
				//Having Heurisitics considering upper bound of dimension
				dimensionToSplit = getDimensionToSplitWithHeuristics(iteration, clusterToSplit);
			} else if(clubsApproach.equals(CLUBSApproach.CLUBS_IN_HIGH_DIMENSIONALITY)) {
				//Case Where the Number of dimensions > total data points
				dimensionToSplit = getDimensionToSplitForLargeDimensionsCase(iteration, clusterToSplit);
			} else if(clubsApproach.equals(CLUBSApproach.CLUBS_STANDARD)) {
				//Standard Case
				dimensionToSplit = getDimensionToSplitStandardCLUBS(iteration, clusterToSplit); 
			} else if(clubsApproach.equals(CLUBSApproach.CLUBS_NEW_APPROACH)) {
				dimensionToSplit = getDimensionToSplitNewApproach(iteration, clusterToSplit);
			}
			
			System.out.println("Iteration: " + iteration + ", Splitting: " + dimensionToSplit.getDimension() 
					+ ", split position = " + dimensionToSplit.getSplitPosition()
					+ ", SSQ: " + dimensionToSplit.getMaxDeltaSSQValue());
			
			if(Math.pow(dimensionToSplit.getMaxDeltaSSQValue(),P) <= meanSSQ0) {
				break;
			} else {
				clusterToSplit = clusterHeap.removeElement();
				Logger.logMessage("CLuster Splitted : " + clusterToSplit.getClusterId() + ", Splitting Dimension for iteration " + iteration + " is: " + dimensionToSplit.getDimension()
										+ "Slitting Position: " + dimensionToSplit.getSplitPosition() + ", Reduction: " + dimensionToSplit.getMaxDeltaSSQValue());
				Cluster[] splittedClusters = splitCluster(clusterToSplit, dimensionToSplit);
				for(Cluster splitCluster : splittedClusters) {
					clusterHeap.addToHeap(splitCluster, splitCluster.calculateClusterSSQ());
				}
			}
			Logger.logMessage("************END******************");
		}
		return getAllClustersFromHeap(clusterHeap);
	}

	private DimensionMetadata getDimensionToSplitNewApproach(int iteration,	Cluster clusterToSplit) {
		MyTree[] listOfCDF = prepareListOfCDF(clusterToSplit);
		List<DataPoint> dataPoints = clusterToSplit.getDataPoints();
		int totalDimensions = clusterToSplit.getTotalDimensions();
		DimensionMetadata[] dimensionMetadataArray = new DimensionMetadata[totalDimensions];
		initDimenstionMetadataArray(dimensionMetadataArray);
		
		int totalDataPoints = clusterToSplit.getTotalDataPoints();
		int startPosition = 0;
		int endPosition;
		for(int window = 1; window <= 20; window++) {
			int nextPercent = 5 * window;
			endPosition = totalDataPoints * nextPercent / 100;
			for(int position = startPosition; position < endPosition; position++) {
				DataPoint dataPointToConsider = dataPoints.get(position);
				for(int dimension = 0; dimension < totalDimensions; dimension++) {
					MyTree treeForDimension = listOfCDF[dimension];
					
					double splitLocation = dataPointToConsider.getCoordinates()[dimension];
					MyTreeValue treeValueLHS = treeForDimension.getCummulativeSum(splitLocation);
					double[] lhs = treeValueLHS.getCoordinates();
					int lhsPoints = treeValueLHS.getTotalPoints();
					
					MyTreeValue treeValueRHS = getRHSTreeValue(treeForDimension, treeValueLHS);
					double[] rhs = treeValueRHS.getCoordinates();
					int rhsPoints = treeValueRHS.getTotalPoints();
					
					double deltaSSQ = SSQ.calculateSSQReduction(lhs, lhsPoints, rhs, rhsPoints);
					dimensionMetadataArray[dimension].updateMaxSSQForPercent(deltaSSQ, nextPercent, splitLocation);
				}
			}
			startPosition = endPosition;
		}
		SpitGraph.writeIterationData(iteration, dimensionMetadataArray, clusterToSplit);
		DimensionMetadata dimensionWithMaxSSQ = getDimensionWithMaxSSQ(dimensionMetadataArray);
		return dimensionWithMaxSSQ;
	}

	private MyTreeValue getRHSTreeValue(MyTree tree, MyTreeValue treeValueLHS) {
		MyTreeValue rightMost = tree.getMaxValue();
		double[] sum = rightMost.getCoordinates().clone();
		double[] lhs = treeValueLHS.getCoordinates();
		
		for(int i = 0; i < sum.length; i++) {
			sum[i] = sum[i] - lhs[i];
		}
		
		return new MyTreeValue(sum, rightMost.getTotalPoints() - treeValueLHS.getTotalPoints());
	}

	private MyTree[] prepareListOfCDF(Cluster clusterToSplit) {
		int totalDimensions = clusterToSplit.getTotalDimensions();
		MyTree[] myTrees = new MyTreeRedBlack[totalDimensions];
		init(myTrees);
		for(DataPoint dataPoint : clusterToSplit.getDataPoints()) {
			double[] coordinates = dataPoint.getCoordinates();
			for(int dimension = 0; dimension < totalDimensions; dimension++) {
				myTrees[dimension].insertValue(coordinates[dimension], coordinates);
			}
		}
		callSumOnTrees(myTrees);
		return myTrees;
	}

	private void callSumOnTrees(MyTree[] myTrees) {
		for(int i = 0; i<myTrees.length; i++) {
			myTrees[i].updateSum();
		}
	}

	private void init(MyTree[] myTrees) {
		for(int i = 0; i<myTrees.length; i++) {
			myTrees[i] = new MyTreeRedBlack();
		}
	}

	private Cluster[] doBottomUpMerging(Cluster[] clusters, double meanSSQ0) {
		updateAdjacentClusters(clusters);
		Map<String, Double> clusterPairSSQGain = new HashMap<String, Double>();
		Map<String, Cluster> clusterIdToClusterMap = buildClusterIdToClusterMap(clusters);
		initMap(clusterPairSSQGain, clusters);
		while(totalNonNullClusters(clusters) > 1) {
			Logger.logMessage("Total Adjacent Pairs: " + clusterPairSSQGain.keySet().size());
			Cluster[] bestPair = getBestPair(clusterPairSSQGain, clusterIdToClusterMap);
			if((getSSQGain(bestPair, clusterPairSSQGain)) > meanSSQ0) {
				break;
			}
			mergeClusters(bestPair[0], bestPair[1]);
			setPositionInArrayAsNull(clusters, bestPair[1]);
			updateClusterPairSSQGain(clusterPairSSQGain, bestPair, clusters);
		}
		return clusters;
	}

	private int totalNonNullClusters(Cluster[] clusters) {
		int totalClusters =0;
		for(Cluster cluster : clusters) {
			if(cluster != null) totalClusters++;
		}
		Logger.logMessage("Total Non null Clusters: " + totalClusters);
		return totalClusters;
	}

	private void updateClusterPairSSQGain(
			Map<String, Double> clusterPairSSQGain, Cluster[] bestPair, Cluster[] currentClusters) {
		for(Cluster adjCluster : bestPair[0].getAdjacentClusters()) {
			if(!isClusterPresent(adjCluster, currentClusters)) continue;
			
			if(adjCluster.getClusterId().equalsIgnoreCase(bestPair[0].getClusterId()) ||
					adjCluster.getClusterId().equalsIgnoreCase(bestPair[1].getClusterId())) continue;
			
			if(bestPair[0].getClusterId().compareTo(adjCluster.getClusterId()) <= 0) { 
				clusterPairSSQGain.put(bestPair[0].getClusterId() + "-" + adjCluster.getClusterId(), 
									SSQ.calculateSSQGainIfMerged(bestPair[0], adjCluster));
			} else {
				clusterPairSSQGain.put(adjCluster.getClusterId() + "-" + bestPair[0].getClusterId(), 
						SSQ.calculateSSQGainIfMerged(bestPair[0], adjCluster));
			}
		}
		/*
		for(Cluster adjCluster : bestPair[1].getAdjacentClusters()) {
			if(!isClusterPresent(adjCluster, currentClusters)) continue;
			
			if(bestPair[1].getClusterId().compareTo(adjCluster.getClusterId()) <= 0) {
				clusterPairSSQGain.remove(bestPair[1].getClusterId() + "-" + adjCluster.getClusterId());
			} else {
				clusterPairSSQGain.remove(adjCluster.getClusterId() + "-" + bestPair[1].getClusterId());
			}
			
			if(bestPair[0].getClusterId().compareTo(adjCluster.getClusterId()) <= 0) {
				clusterPairSSQGain.put(bestPair[0].getClusterId() + "-" + adjCluster.getClusterId(), 
						SSQ.calculateSSQGainIfMerged(bestPair[0], adjCluster));
			} else {
				clusterPairSSQGain.put(adjCluster.getClusterId() + "-" + bestPair[0].getClusterId(), 
						SSQ.calculateSSQGainIfMerged(bestPair[0], adjCluster));
			}
		}
		*/
		removeAllMapEntriesContainingKey(bestPair[1].getClusterId(), clusterPairSSQGain);
		clusterPairSSQGain.remove(bestPair[0].getClusterId() + "-" + bestPair[0].getClusterId());
	}

	private boolean isClusterPresent(Cluster adjCluster,
			Cluster[] currentClusters) {
		for(Cluster cluster : currentClusters) {
			if(cluster == null) continue;
			if(cluster.getClusterId().equals(adjCluster.getClusterId())) return true;
		}
		return false;
	}

	private void removeAllMapEntriesContainingKey(String clusterId, Map<String, Double> clusterPairSSQGain) {
		Iterator<Map.Entry<String,Double>> iter = clusterPairSSQGain.entrySet().iterator();
		while (iter.hasNext()) {
		    Map.Entry<String,Double> entry = iter.next();
		    if(entry.getKey().startsWith(clusterId + "-") ||
		    		entry.getKey().endsWith("-" + clusterId)){
		        iter.remove();
		    }
		}
		
	}

	private void mergeClusters(Cluster cluster1, Cluster cluster2) {
		cluster1.addAsAdjacentClusters(cluster2.getAdjacentClusters());
		cluster1.addDataPoints(cluster2.getDataPoints());
		cluster1.updateRange(cluster2);
	}

	private void setPositionInArrayAsNull(Cluster[] clusters, Cluster cluster) {
		Logger.logMessage("Setting Cluster as null : " + cluster.getClusterId());
		for(int i = 0; i<clusters.length; i++) {
			if(clusters[i] == null) continue;
			
			if(clusters[i].getClusterId().equals(cluster.getClusterId())) {
				clusters[i] = null;
				break;
			}
		}
	}

	private double getSSQGain(Cluster[] bestPair,
			Map<String, Double> clusterPairSSQGain) {
		return clusterPairSSQGain.get(bestPair[0].getClusterId() + "-" + bestPair[1].getClusterId());
	}

	private Map<String, Cluster> buildClusterIdToClusterMap(Cluster[] clusters) {
		Map<String, Cluster> newMap = new HashMap<String, Cluster>();
		for(Cluster cluster : clusters) {
			newMap.put(cluster.getClusterId(), cluster);
		}
		return newMap;
	}

	private Cluster[] getBestPair(Map<String, Double> clusterPairSSQGain,
									Map<String, Cluster> clusterIdToClusterMap) {
		String key = getKeyWithMinValue(clusterPairSSQGain);
		String[] clusterIds = key.split("-");
		Logger.logMessage(key);
		return new Cluster[] {clusterIdToClusterMap.get(clusterIds[0]),
								clusterIdToClusterMap.get(clusterIds[1])};
	}

	private String getKeyWithMinValue(Map<String, Double> map) {
		double minValue = Double.MAX_VALUE;
		String minKey = "";
		for(String key : map.keySet()) {
			double value = map.get(key);
			if(value < minValue) {
				minValue = value;
				minKey = key;
			}
		}
		return minKey;
	}

	private void initMap(Map<String, Double> clusterPairSSQGain,
			Cluster[] clusters) {
		for(Cluster cluster : clusters) {
			for(Cluster adjCluster : cluster.getAdjacentClusters()) {
				double deltaSSQ = SSQ.calculateSSQGainIfMerged(cluster, adjCluster);
				if(cluster.getClusterId().compareTo(adjCluster.getClusterId()) <= 0) {
					clusterPairSSQGain.put(cluster.getClusterId() + "-" + adjCluster.getClusterId(), deltaSSQ);
				} else {
					clusterPairSSQGain.put(adjCluster.getClusterId() + "-" + cluster.getClusterId(), deltaSSQ);
				}
			}
		}
	}

	private void updateAdjacentClusters(Cluster[] clusters) {
		for(int i = 0; i< clusters.length; i++) {
			for(int j = i+1; j < clusters.length; j++) {
					if (clusters[i].isAdjacentCluster(clusters[j])) {
						clusters[i].addAsAdjacentCluster(clusters[j]);
						clusters[j].addAsAdjacentCluster(clusters[i]);
					}
			}
		}
	}

	
	private Cluster[] getAllClustersFromHeap(MyHeap<Cluster, Double> clusterHeap) {
		Cluster[] clusters = new Cluster[clusterHeap.size()];
		int position = 0;
		
		while(clusterHeap.size()  > 0) {
			Cluster cluster = clusterHeap.removeElement();
			clusters[position] = cluster;
			position++;
		}
		return clusters;
	}

	private Cluster[] splitCluster(Cluster clusterToSplit, DimensionMetadata dimensionToSplit) {
		Cluster[] clusterArray = new Cluster[2];
		int totalDimensions = clusterToSplit.getTotalDimensions();
		Cluster clusterLeft = new Cluster(totalDimensions, clusterToSplit.getClusterId() + "1");
		Cluster clusterRight = new Cluster(totalDimensions, clusterToSplit.getClusterId() + "2");
		clusterArray[0] = clusterLeft;
		clusterArray[1] = clusterRight;
		
		int dimensionNumberToSplit = dimensionToSplit.getDimension();
		for(DataPoint dataPoint : clusterToSplit.getDataPoints()) {
			if(dataPoint.getCoordinates()[dimensionNumberToSplit] <= dimensionToSplit.getSplitPosition()) {
				clusterLeft.addDataPoint(dataPoint);
			} else {
				clusterRight.addDataPoint(dataPoint);
			}
		}
		ClusterMetadata clusterMetadataLeft = clusterToSplit.getClusterMetadata().getClusterMetadataCopyForSplittedCluster();
		clusterLeft.setClusterMetadata(clusterMetadataLeft);
		Range[] leftRange = clusterToSplit.getClusterRanges();
		leftRange[dimensionNumberToSplit].setUpperBound(dimensionToSplit.getSplitPosition());
		clusterLeft.setRange(leftRange);
		
		ClusterMetadata clusterMetadataRight = clusterToSplit.getClusterMetadata().getClusterMetadataCopyForSplittedCluster();
		clusterRight.setClusterMetadata(clusterMetadataRight);
		Range[] rightRange = clusterToSplit.getClusterRanges();
		rightRange[dimensionNumberToSplit].setLowerBound(dimensionToSplit.getSplitPosition());
		clusterRight.setRange(rightRange);
	
		return clusterArray;
	}


	private DimensionMetadata getDimensionToSplitStandardCLUBS(int iteration, Cluster clusterToSplit) {
		DimensionMetadata maxDimensionMetadata = null;
		int totalDimensionsReevaluated = 0;
		ClusterMetadata clusterMetadata = clusterToSplit.getClusterMetadata();
		for(int dimension = 0; dimension < clusterToSplit.getTotalDimensions(); dimension++) {
			totalDimensionsReevaluated++;
			DimensionMetadata dimensionMetadata = SSQ.calculateDimensionMetadaNew(clusterToSplit, dimension);
			clusterMetadata.addDimensionMetadata(dimensionMetadata);
			if((maxDimensionMetadata == null) || 
					(maxDimensionMetadata.getMaxDeltaSSQValue() < dimensionMetadata.getMaxDeltaSSQValue())) {
				maxDimensionMetadata = dimensionMetadata;
			}
		}
		
		Logger.logMessage("Total Dimensions evaluated for iteration " + iteration + ", is: " + totalDimensionsReevaluated 
					+ "/" + clusterToSplit.getTotalDimensions());
		return maxDimensionMetadata;
	}
	
	private DimensionMetadata getDimensionToSplitWithHeuristics(int iteration, Cluster clusterToSplit) {
		ClusterMetadata clusterMetadata = clusterToSplit.getClusterMetadata();
		DimensionMetadata dimension = clusterMetadata.getDimensionWithMaxSSQ();
		int totalDimensionsReevaluated = 0;
		while(dimension.isSSQValueEstimated()) {
			totalDimensionsReevaluated++;
			dimension = clusterMetadata.removeDimensionWithMaxSSQ();
			DimensionMetadata updatedDimensionMetadata = SSQ.calculateDimensionMetadaNew(clusterToSplit, dimension.getDimension());
			clusterMetadata.addDimensionMetadata(updatedDimensionMetadata);
			dimension = clusterMetadata.getDimensionWithMaxSSQ();
		}
		Logger.logMessage("Total Dimensions evaluated for iteration " + iteration + ", is: " + totalDimensionsReevaluated 
									+ "/" + clusterToSplit.getTotalDimensions());
		return dimension;
	}
	

	private DimensionMetadata getDimensionToSplitForLargeDimensionsCase(int iteration,	Cluster clusterToSplit) {
		List<DataPoint> dataPoints = clusterToSplit.getDataPoints();
		int totalDimensions = clusterToSplit.getTotalDimensions();
		DimensionMetadata[] dimensionMetadataArray = new DimensionMetadata[totalDimensions];
		initDimenstionMetadataArray(dimensionMetadataArray);
		
		int totalDataPoints = clusterToSplit.getTotalDataPoints();
		int startPosition = 0;
		int endPosition;
		for(int window = 1; window <= 20; window++) {
			int nextPercent = 5 * window;
			endPosition = totalDataPoints * nextPercent / 100;
			for(int position = startPosition; position < endPosition; position++) {
				DataPoint dataPointToConsider = dataPoints.get(position);
				for(int dimension = 0; dimension < totalDimensions; dimension++) {
					double splitLocation = dataPointToConsider.getCoordinates()[dimension];
					double deltaSSQ = SSQ.calculateDeltaSSQ(clusterToSplit, dimension, splitLocation);
					dimensionMetadataArray[dimension].updateMaxSSQForPercent(deltaSSQ, nextPercent, splitLocation);
				}
			}
			startPosition = endPosition;
		}
		SpitGraph.writeIterationData(iteration, dimensionMetadataArray, clusterToSplit);
		DimensionMetadata dimensionWithMaxSSQ = getDimensionWithMaxSSQ(dimensionMetadataArray);
		return dimensionWithMaxSSQ;
	}

	private void initDimenstionMetadataArray(
			DimensionMetadata[] dimensionMetadataArray) {
		for(int i = 0; i< dimensionMetadataArray.length; i++) {
			dimensionMetadataArray[i] = new DimensionMetadata(i, false, -1 * Double.MIN_VALUE, 
													-1 * Double.MIN_VALUE);
		}
		
	}

	private DimensionMetadata getDimensionWithMaxSSQ(DimensionMetadata[] dimensionMetadataArray) {
		DimensionMetadata max = dimensionMetadataArray[0];
		for(int i = 1; i < dimensionMetadataArray.length; i++) {
			DimensionMetadata current = dimensionMetadataArray[i];
			if(current.getMaxDeltaSSQValue() > max.getMaxDeltaSSQValue() ) {
				max = current;
			}
		}
		return max;
	}
}
