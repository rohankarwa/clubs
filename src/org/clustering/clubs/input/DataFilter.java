package org.clustering.clubs.input;

public interface DataFilter {

	public boolean filterData(String[] inputData);
}
