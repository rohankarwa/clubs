package org.clustering.clubs.input;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.clustering.clubs.Cluster;
import org.clustering.clubs.DataPoint;
import org.clustering.clubs.Range;
import org.file.MyFile;

public class InputDataManagerImpl implements InputDataManager {

	@Override
	public Cluster loadFileDataAsSingleCluster(String fileName, String tokenizer, int idColumn) {
		int totalDimensions = 0;
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		try {
			MyFile myFile = new MyFile(fileName);
			Iterator<String> fileIterartor = myFile.iterator();
			String line = "";
			while(fileIterartor.hasNext()) {
				line = fileIterartor.next();
				String[] lineContent = line.split(tokenizer);
				
				double[] coordinates = getCoordinates(lineContent, idColumn);
				String id = (lineContent[idColumn]);
				DataPoint datapoint = new DataPoint(id, coordinates);
				dataPoints.add(datapoint);
			}
			totalDimensions = line.split(tokenizer).length - 1;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Cluster cluster = new Cluster(totalDimensions, "1");
		cluster.addDataPoints(dataPoints);
		Range[] range = buildDefaultRange(totalDimensions);
		cluster.setRange(range);
		
		return cluster;
	}

	private Range[] buildDefaultRange(int totalDimensions) {
		Range[] range = new Range[totalDimensions];
		for(int i = 0; i< totalDimensions; i++) {
			range[i] = new Range(Double.MAX_VALUE * -1, Double.MAX_VALUE);
		}
		return range;
	}

	private double[] getCoordinates(String[] lineContent, int columnToSkip) {
		double[] coordinates = new double[lineContent.length - 1];
		int k = 0;
		for(int i=0; i<lineContent.length; i++) {
			if(i == columnToSkip) continue;
			coordinates[k] = Double.parseDouble(lineContent[i].trim());
			k++;
		}
		return coordinates;
	}

}
