package org.clustering.clubs.input;

import org.clustering.clubs.Cluster;

public interface InputDataManager {

	public Cluster loadFileDataAsSingleCluster(String fileName, String tokenizer, int idColumn);
}
