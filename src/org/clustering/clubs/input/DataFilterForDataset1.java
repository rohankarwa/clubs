package org.clustering.clubs.input;

import java.util.List;
import java.util.Map;

public class DataFilterForDataset1 implements DataFilter {

	private Map<Integer, List<String>> filterMap;
	public DataFilterForDataset1(Map<Integer, List<String>> filterMap) {
		this.filterMap = filterMap;
	}
	
	@Override
	public boolean filterData(String[] inputData) {
		for(int filterPosition : filterMap.keySet()) {
			List<String> skippingSet = filterMap.get(filterPosition);
			if(skippingSet.contains(inputData[filterPosition])) {
				System.out.println(skippingSet.size());
				skippingSet.remove(inputData[filterPosition]);
				return true;
			}
		}
		return false;
	}
}
