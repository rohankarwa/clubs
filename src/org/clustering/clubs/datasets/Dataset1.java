package org.clustering.clubs.datasets;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.clustering.clubs.input.DataFilter;
import org.clustering.clubs.input.DataFilterForDataset1;
import org.file.MyFile;
import org.file.MyFileWriter;

public class Dataset1 {

	private String inputFile;
	private String outputFile;
	private DataFilter dataFilter;
	public Dataset1(String dataset1File, String newFile) {
		this.inputFile = dataset1File;
		this.outputFile = newFile;
		this.dataFilter = createDataFilterForDataset1();
	}
	
	private DataFilter createDataFilterForDataset1() {
		String fileName = "/Users/rohan/Desktop/UCLA Stuff/Course Work/Master Project/Dataset/Dataset1/All98Genes.txt";
		try {
			List<String> skippingSet = new ArrayList<String>();
			MyFile myFile = new MyFile(fileName);
			Iterator<String> iterator = myFile.iterator();
			while(iterator.hasNext()) {
				String content = iterator.next().trim();
				if(skippingSet.contains(content)) {
					System.out.println("Duplicate: " + content);
				}
				skippingSet.add(content);
			}
			Map<Integer, List<String>> filterMap = new HashMap<Integer, List<String>>();
			filterMap.put(0, skippingSet);
			return new DataFilterForDataset1(filterMap);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void doFilter() throws IOException {
		MyFileWriter myFileWriter = new MyFileWriter(outputFile);
		MyFile myFile = new MyFile(inputFile);
		Iterator<String> fileIterartor = myFile.iterator();
		String line = "";
		while(fileIterartor.hasNext()) {
			line = fileIterartor.next();
			String[] lineContent = line.split(",");
			if(!dataFilter.filterData(lineContent)) {
				continue;
			}
			myFileWriter.writeLine(line);
		}
		myFileWriter.close();
	}
	
	public static void main(String[] args) {
		String inputFile = "/Users/rohan/Desktop/UCLA Stuff/Course Work/Master Project/Dataset/Dataset1/Dataset1.csv";
		String outputFile = "/Users/rohan/Desktop/UCLA Stuff/Course Work/Master Project/Dataset/Dataset1/FilteredDataset1.csv";
		Dataset1 dataset1 = new Dataset1(inputFile, outputFile);
		try {
			dataset1.doFilter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
