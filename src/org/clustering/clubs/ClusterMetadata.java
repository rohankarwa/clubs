package org.clustering.clubs;

public interface ClusterMetadata {

	public DimensionMetadata getDimensionWithMaxSSQ();
	
	public DimensionMetadata getDimensionMetadata(int dimension);
	
	public DimensionMetadata removeDimensionWithMaxSSQ();
	
	public void addDimensionMetadata(DimensionMetadata dimensionMetadata);

	public ClusterMetadata getClusterMetadataCopyForSplittedCluster();
}

