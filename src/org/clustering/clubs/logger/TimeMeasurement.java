package org.clustering.clubs.logger;

import java.io.IOException;

import org.file.MyFileWriter;

public class TimeMeasurement {
	private static MyFileWriter myFileWriter = null;
	private static final String fileName = "./time/time.log";
	static {
		try {
			myFileWriter = new MyFileWriter(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void logTime(String message) {		
		try {
			myFileWriter.writeLine(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void closeFile() {
		try {
			myFileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
