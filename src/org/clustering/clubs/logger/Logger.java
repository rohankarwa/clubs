package org.clustering.clubs.logger;

import java.io.IOException;

import org.file.MyFileWriter;

public class Logger {

	private static MyFileWriter myFileWriter = null;
	private static final String fileName = "./test-data/output.log";
	static {
		try {
			myFileWriter = new MyFileWriter(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void logMessage(String message) {		
		try {
			myFileWriter.writeLine(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void closeFile() {
		try {
			myFileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
