package org.clustering.clubs.logger;

import java.io.IOException;

import org.clustering.clubs.Cluster;
import org.clustering.clubs.DimensionMetadata;
import org.file.MyFileWriter;

public class SpitGraph {

	private static MyFileWriter myFileWriter = null;
	private static final String fileName = "./test-data/process_graph.txt";
	static {
		try {
			myFileWriter = new MyFileWriter(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void writeIterationData(int iteration, DimensionMetadata[] dimensionMetadataArray, Cluster cluster) {		
		try {
			myFileWriter.writeLine("");
			myFileWriter.writeLine("");
			myFileWriter.writeLine("Data for Iteration: " + iteration + ". Total Points in the cluster : " + cluster.getTotalDataPoints());
			myFileWriter.writeLine(buildHeaderLine());
			for(int i = 0; i<dimensionMetadataArray.length; i++) {
				myFileWriter.writeLine("Dimension " + i + "\t" + getProgressPoints(dimensionMetadataArray[i].getMaxSSQValueDataProgress()));
			}
			myFileWriter.writeLine("");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static String getProgressPoints(double[] progress) {
		StringBuilder builder = new StringBuilder();
		for(double value: progress) {
			builder.append(value + "\t");
		}
		return builder.toString();
	}

	private static String buildHeaderLine() {
		StringBuilder builder = new StringBuilder();
		for(int i = 5; i <=100; i = i + 5) {
			builder.append("\tPercent " + i);
		}
		return builder.toString();
		
	}

	public static void closeFile() {
		try {
			myFileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
