package org.clustering.clubs.main;

import java.util.ArrayList;
import java.util.List;

import org.clustering.clubs.CLUBS;
import org.clustering.clubs.CLUBSApproach;
import org.clustering.clubs.CLUBSImpl;
import org.clustering.clubs.CLUBSImplNew;
import org.clustering.clubs.Cluster;
import org.clustering.clubs.input.InputDataManager;
import org.clustering.clubs.input.InputDataManagerImpl;
import org.clustering.clubs.logger.Logger;
import org.clustering.clubs.logger.SpitGraph;
import org.clustering.clubs.logger.TimeMeasurement;
import org.clustering.quality.QualityMeasurement;

public class MainClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {	
		
		List<String> files = getListOfFiles();
		
		for(String file : files) {
		//	executeGeneratedData(file);
		
			executeCLUBSNewImplGeneratedData(file);
			TimeMeasurement.logTime("");
		}
		
		TimeMeasurement.closeFile();
		Logger.closeFile();
		SpitGraph.closeFile();
	}

	private static List<String> getListOfFiles() {
		List<String> files = new ArrayList<String>();
		/*
		files.add("./test-data/suite/generated-unique-10d-10s-100000p/prova10d_10s_100000p.dataWithId.txt");
		
		files.add("./test-data/suite/generated-uniq-20d-20s-100000p/prova20d_20s_100000p.dataWithId.txt");
		
		files.add("./test-data/suite/generated-uniq-30d-30s-100000p/prova30d_30s_100000p.dataWithId.txt");
		files.add("./test-data/suite/generated-uniq-40d-40s-100000p/prova40d_40s_100000p.dataWithId.txt");
		files.add("./test-data/suite/generated-uniq-50d-50s-100000p/prova50d_50s_100000p.dataWithId.txt");
		files.add("./test-data/suite/generated-uniq-100d-100s-100000p/prova100d_100s_100000p.dataWithId.txt");
		*/
		//files.add("./test-data/suite/50d-50s-30c-100000p/prova50d_50s_30c_100000p.dataWithId.txt");
		
		/*
		//Unique
		files.add("./test-data/suite/unique/prova_unique_10d_10s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/unique/prova_unique_20d_20s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/unique/prova_unique_30d_30s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/unique/prova_unique_40d_40s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/unique/prova_unique_50d_50s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/unique/prova_unique_100d_100s_10c_100000p.dataWithId.txt");
		*/
		
		//Not Unique
		files.add("./test-data/suite/not-unique/prova_not_unique_10d_10s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/not-unique/prova_not_unique_20d_20s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/not-unique/prova_not_unique_30d_30s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/not-unique/prova_not_unique_40d_40s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/not-unique/prova_not_unique_50d_50s_10c_100000p.dataWithId.txt");
		files.add("./test-data/suite/not-unique/prova_not_unique_100d_100s_10c_100000p.dataWithId.txt");
		
		return files;
	}

	private static void executeGeneratedData(String dataFile) {
		Logger.logMessage("Doing Analysis for File: " + dataFile);
		InputDataManager inputDataManager = new InputDataManagerImpl();
		
		CLUBS clubs = new CLUBSImpl(inputDataManager, CLUBSApproach.CLUBS_STANDARD);
		Cluster[] cluster = clubs.doClustering(dataFile, ",", 0);
		Logger.logMessage("Total Clusters: " + cluster.length);
		printClusterArray(cluster);
		QualityMeasurement qualityMeasurement = new QualityMeasurement();
		qualityMeasurement.doQualityCheck(cluster, 10000);
		
	}
	
	private static void executeCLUBSNewImplGeneratedData(String dataFile) {
		Logger.logMessage("Doing Analysis for File: " + dataFile);
		InputDataManager inputDataManager = new InputDataManagerImpl();
		
		CLUBS clubs = new CLUBSImplNew(inputDataManager, CLUBSApproach.CLUBS_NEW_APPROACH);
		Cluster[] cluster = clubs.doClustering(dataFile, ",", 0);
		Logger.logMessage("Total Clusters: " + cluster.length);
		printClusterArray(cluster);
		QualityMeasurement qualityMeasurement = new QualityMeasurement();
		qualityMeasurement.doQualityCheck(cluster, 10000);
	}
	
	private static void printClusterArray(Cluster[] cluster) {
		int k = 1;
		for(int i = 0; i< cluster.length; i++) {
			if(cluster[i] == null) {
				Logger.logMessage("Skipping");
				continue;
			}
			Logger.logMessage("Cluster " + k + ": " + cluster[i].getDataPoints());
			k++;
		}
		
	}
	
	/*
	private static void executeDataset1() {
		//String dataFile = "/Users/rohan/Desktop/UCLA Stuff/Course Work/Master Project/Dataset/Dataset1/fresh/FilteredDataset1_without_duplicateTranspose.csv";
		
		String dataFile = "/Users/rohan/Desktop/UCLA Stuff/Course Work/Master Project/Dataset/Dataset1/fresh/weka/FilteredDataset1Transpose_standardize.arff";
		Logger.logMessage("Doing Analysis for File: " + dataFile);
		InputDataManager inputDataManager = new InputDataManagerImpl();
		
		CLUBS clubs = new CLUBSImpl(inputDataManager, CLUBSApproach.CLUBS_HEURISITICS_UPPER_BOUND);
		Cluster[] cluster = clubs.doClustering(dataFile, ",", 0);
		Logger.logMessage("Total Clusters: " + cluster.length);
		printClusterArray(cluster);
		QualityMeasurement qualityMeasurement = new QualityMeasurement();
		qualityMeasurement.doQualityCheck(cluster);
		Logger.closeFile();
		SpitGraph.closeFile();
		
	}
	*/

	/*
	private static void executeNormalWay() {
		String dataFile = "./test-data/suite/glass/normalizedGlass1.arff";
		int idColumn = 9;
		Logger.logMessage("Doing Analysis for File: " + dataFile);
		InputDataManager inputDataManager = new InputDataManagerImpl();
		
		CLUBS clubs = new CLUBSImpl(inputDataManager, CLUBSApproach.CLUBS_HEURISITICS_UPPER_BOUND);
		Cluster[] cluster = clubs.doClustering(dataFile, ",", idColumn);
		Logger.logMessage("Total Clusters: " + cluster.length);
		printClusterArray(cluster);
		QualityMeasurement qualityMeasurement = new QualityMeasurement();
		qualityMeasurement.doQualityCheck(cluster);
		Logger.closeFile();
		SpitGraph.closeFile();
	}*/

}
