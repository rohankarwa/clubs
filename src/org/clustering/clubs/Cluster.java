package org.clustering.clubs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Cluster {
	
	private final String clusterId;
	private ClusterMetadata clusterMetadata;
	private List<DataPoint> dataPoints = new ArrayList<DataPoint>();
	private int totalDimensions;
	private double[] linearSum;
	private double[] squareSum;
	private Set<Cluster> adjacentClusters = new HashSet<Cluster>();
	private Range[] rangeForDimensions;
	
	public Cluster(int totalDimensions, String clusterId) {
		this.clusterId = clusterId;
		this.totalDimensions = totalDimensions;
		linearSum = new double[totalDimensions];
		squareSum = new double[totalDimensions];
		rangeForDimensions = new Range[totalDimensions];
	}
	
	public void configureClusterMetadata() {
		for(int dimension = 0; dimension<totalDimensions; dimension++) {
			DimensionMetadata dimensionMetadata = SSQ.calculateDimensionMetadaNew(this, dimension);
			clusterMetadata.addDimensionMetadata(dimensionMetadata);
		}
	}
	
	public void setClusterMetadata(ClusterMetadata clusterMetadata) {
		this.clusterMetadata = clusterMetadata;
	}
	
	public void setRange(Range[] rangeForDimension) {
		this.rangeForDimensions = rangeForDimension;
	}
	
	public void addDataPoint(DataPoint dataPoint) {
		this.dataPoints.add(dataPoint);
		double[] coordinates = dataPoint.getCoordinates();
		for(int dimension = 0; dimension < coordinates.length; dimension++) {
			linearSum[dimension] += coordinates[dimension];
			squareSum[dimension] += Math.pow(coordinates[dimension], 2);
		}
	}
	
	public Range[] getClusterRanges() {
		return cloneRange(rangeForDimensions);
	}
	
	
	public void updateRange(Cluster otherCluster) {
		for(int dimension = 0; dimension < totalDimensions; dimension++) {
			rangeForDimensions[dimension].setLowerBound(Math.min(rangeForDimensions[dimension].getLowerBound(), 
																otherCluster.rangeForDimensions[dimension].getLowerBound()));
			rangeForDimensions[dimension].setUpperBound(Math.max(rangeForDimensions[dimension].getUpperBound(), 
																otherCluster.rangeForDimensions[dimension].getUpperBound()));
		}
	}
	
	private Range[] cloneRange(Range[] rangeForDimensions) {
		Range[] clonedRange = new Range[rangeForDimensions.length];
		for(int i = 0; i<rangeForDimensions.length;i++) {
			if(rangeForDimensions[i] == null) {
				clonedRange[i] = null;
			} else {
				clonedRange[i] = new Range(rangeForDimensions[i].getLowerBound(),
											rangeForDimensions[i].getUpperBound());
			}
		}
		return clonedRange;
	}

	public double calculateClusterSSQ() {
		return SSQ.calculateSSQ(this);
	}
	
	public void addDataPoints(List<DataPoint> dataPoints) {
		for(DataPoint dataPoint : dataPoints) {
			addDataPoint(dataPoint);
		}
	}
	
	public List<DataPoint> getDataPoints() {
		//no need to clone as data points is not mutable
		//return clone(dataPoints);
		
		return dataPoints;
	}
	
	/*
	private List<DataPoint> clone(List<DataPoint> dataPoints) {
		List<DataPoint> newListofDataPoints = new ArrayList<DataPoint>(); 
		newListofDataPoints.addAll(dataPoints);
		return newListofDataPoints;
	}
	*/

	public int getTotalDimensions() {
		return totalDimensions;
	}
	
	public double getLinearForDimension(int dimension) {
		return linearSum[dimension];
	}
	
	public double getSquareSumForDimension(int dimension) {
		return squareSum[dimension];
	}
	
	public double[] getLinearSum() {
		return clone(linearSum);
	}

	public double[] getSquareSum() {
		return clone(squareSum);
	}
	
	public int getTotalDataPoints() {
		return dataPoints.size();
	}
	
	public ClusterMetadata getClusterMetadata() {
		return clusterMetadata;
	}
	
	public String getClusterId() {
		return clusterId;
	}
	
	public Set<Cluster> getAdjacentClusters() {
		return adjacentClusters;
	}
	
	public void addAsAdjacentCluster(Cluster cluster) {
		if(cluster.getClusterId().equalsIgnoreCase(clusterId)) return;
		
		adjacentClusters.add(cluster);
	}
	
	public void addAsAdjacentClusters(Set<Cluster> clusters) {
		for(Cluster cluster : clusters) {
			addAsAdjacentCluster(cluster);
		}
	}
	
	public boolean isAdjacentCluster(Cluster otherCluster) {
		for(int dimension = 0; dimension < totalDimensions; dimension++) {
			if(! doesRangeInteresect(this.rangeForDimensions[dimension],
										otherCluster.rangeForDimensions[dimension])) {
				return false;
			}
		}
		return true;
	}
	
	private boolean doesRangeInteresect(Range range1, Range range2) {
		range1.sanity();
		range2.sanity();
		
		if((range1.getLowerBound() > range2.getUpperBound()) ||
				range1.getUpperBound() < range2.getLowerBound()) {
			return false;
		} else {
			return true;
		}
	}

	private double[] clone(double[] array) {
		double[] cloneCopy = new double[array.length];
		for(int i = 0; i < array.length; i++) {
			cloneCopy[i] = array[i];
		}
		return cloneCopy;
	}
	
	@Override
	public String toString() {
		return clusterId;
	}
	
	public void shuffleDataPoints() {
		Collections.shuffle(dataPoints);
	}
	
	public List<DataPoint> getDataPoints(int totalPointsToReturn) {
		//int totalPointsToReturn = (int) (dataPoints.size() * percent / 100);
		List<DataPoint> pointsToReturn = new ArrayList<DataPoint>();
		for(int i = 0; i< totalPointsToReturn; i++) {
			pointsToReturn.add(dataPoints.get(i));
		}
		return pointsToReturn;
	}
}
