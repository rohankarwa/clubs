package org.clustering.quality;

import org.clustering.clubs.Cluster;
import org.clustering.clubs.DataPoint;
import org.clustering.clubs.logger.Logger;
import org.clustering.clubs.logger.TimeMeasurement;

import java.util.*;

public class QualityMeasurement {

	private Map<String, Integer> positiveMap = new HashMap<String, Integer>();
	private Map<String, Integer> negativeMap = new HashMap<String, Integer>();
	
	public void doQualityCheck(Cluster[] clusters, int totalPointsInEachCluster) {
		int totalError = 0;
		double totalSSQ = 0;
		clusters = removeNullClusters(clusters);
		Logger.logMessage("Total Clusters: " + clusters.length);
			
		for(Cluster cluster : clusters) {
			String dominantId = getDominantId(cluster);
			updateMaps(dominantId, cluster);
			totalSSQ += cluster.calculateClusterSSQ();
			if(cluster.getTotalDataPoints() > totalPointsInEachCluster) {
				totalError += (cluster.getTotalDataPoints() - totalPointsInEachCluster);
			}
		}
		
		System.out.println("Total SSQ = " + totalSSQ);
		Logger.logMessage("Total SSQ = " + totalSSQ);
		TimeMeasurement.logTime("Total SSQ = " + totalSSQ);
		TimeMeasurement.logTime("Total Error = " + totalError);
		//printMapContent();
	}

	private void printMapContent() {
		int globalPositive = 0;
		int globalNegative = 0;
		for(String id : positiveMap.keySet()) {
			int positiveValue = positiveMap.get(id);
			int negativeValue = negativeMap.get(id);
			int total = positiveValue + negativeValue;
			double positivePercent = (double)positiveValue/total * 100;
			double negativePercent = (double)negativeValue/total * 100;
			System.out.println("Class with Id: " + id + ", Total: " + total 
					+ "Rightly Classified: " + positiveValue + " ("+positivePercent+"), " 
					+ "Wrongly Classified: " + negativeValue + " ("+negativePercent+"), ");
			Logger.logMessage("Class with Id: " + id + ", Total: " + total 
					+ "Rightly Classified: " + positiveValue + " ("+positivePercent+"), " 
					+ "Wrongly Classified: " + negativeValue + " ("+negativePercent+"), ");
			globalPositive += positiveValue;
			globalNegative += negativeValue;
		}
		int globalTotal = globalNegative + globalPositive;
		double globalPositivePercent = (double)globalPositive/globalTotal * 100;
		double globalNegativePercent = (double)globalNegative/globalTotal * 100;
		System.out.println("Global, Total: " + globalTotal 
				+ "Rightly Classified: " + globalPositive + " ("+globalPositivePercent+"), " 
				+ "Wrongly Classified: " + globalNegative + " ("+globalNegativePercent+"), ");
		Logger.logMessage("Global, Total: " + globalTotal 
				+ "Rightly Classified: " + globalPositive + " ("+globalPositivePercent+"), " 
				+ "Wrongly Classified: " + globalNegative + " ("+globalNegativePercent+"), ");
	}

	private void updateMaps(String dominantId, Cluster cluster) {
		int total = 0;
		int trueValues = 0;
		for(DataPoint dataPoint : cluster.getDataPoints()) {
			String dataPointId = dataPoint.getId();
			if(!positiveMap.containsKey(dataPointId)) {
				positiveMap.put(dataPointId, 0);
			}
			if(!negativeMap.containsKey(dataPointId)) {
				negativeMap.put(dataPointId, 0);
			}
			
			if(dataPointId.equals(dominantId)) {
				positiveMap.put(dataPointId, positiveMap.get(dataPointId) + 1);
				trueValues++;
			} else {
				negativeMap.put(dataPointId, negativeMap.get(dataPointId) + 1);
			}
			total++;
		}
		System.out.println("Cluster Purity : " + cluster.getClusterId() + ", " 
							+ "(" + trueValues + "/" + total + ") = " + (double)trueValues/total *100);
		Logger.logMessage("Cluster Purity : " + cluster.getClusterId() + ", " 
				+ "(" + trueValues + "/" + total + ") = " + (double)trueValues/total *100);
	}

	private String getDominantId(Cluster cluster) {
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for(DataPoint dataPoint : cluster.getDataPoints()) {
			String dataPointId = dataPoint.getId();
			if(!countMap.containsKey(dataPointId)) {
				countMap.put(dataPointId, 1);
			} else {
				countMap.put(dataPointId, countMap.get(dataPointId) + 1);
			}
		}
		
		return getKeyWithMaxValue(countMap);
	}
	
	private String getKeyWithMaxValue(Map<String, Integer> countMap) {
		String maxKey = "";
		double maxValue = -1;
		
		for(String key : countMap.keySet()) {
			int value = countMap.get(key);
			if(value > maxValue) {
				maxKey = key;
				maxValue = value;
			}
		}
		return maxKey;
	}

	/*
	private int getTotalUniqueIds(Cluster[] clusters) {
		Set<Double> uniqueIds = new HashSet<Double>();
		for(Cluster cluster : clusters) {
			for(DataPoint dataPoint : cluster.getDataPoints()) {
				uniqueIds.add(dataPoint.getId());
			}
		}
		return uniqueIds.size();
	}*/

	private Cluster[] removeNullClusters(Cluster[] clusters) {
		int totalNonNullClusters = calculatedNonNullClusters(clusters);
		Cluster[] newClusterArray = new Cluster[totalNonNullClusters];
		int k = 0;
		for(Cluster cluster : clusters) {
			if(cluster == null) continue;
			
			newClusterArray[k] = cluster;
			k++;
		}
		return newClusterArray;
	}

	private int calculatedNonNullClusters(Cluster[] clusters) {
		int count = 0;
		for(Cluster cluster : clusters) {
			if(cluster != null) count++;
		}
		return count;
	}

}
