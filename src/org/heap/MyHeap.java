package org.heap;

public class MyHeap<T, E extends Comparable<E>> {

	private HeapElement<T,E>[] heapArray;
	private int maxHeapSize;
	private int heapSize = 0;
	private boolean isMinHeap;
	
	public MyHeap(int maxHeapSize, boolean isMinHeap) {
		this.maxHeapSize = maxHeapSize;
		heapArray = new HeapElement[this.maxHeapSize + 1];
		this.isMinHeap = isMinHeap;
	}
	
	public void addToHeap(T item, E priority) {
		HeapElement<T,E> heapElement = new HeapElement<T, E>(priority, item);
		heapArray[heapSize + 1] = heapElement;
		swim(heapSize + 1);
		heapSize++;
	}
	
	public T removeElement() {
		T elementData = (T)heapArray[1].data;
		heapArray[1] = heapArray[heapSize];
		heapArray[heapSize ] = null;
		heapSize--;
		sink(1);
		
		return elementData;
	}
	
	public T peek() {
		return (T)heapArray[1].data;
	}
	
	public int size() {
		return heapSize;
	}
	
	private void swim(int position) {
		if(isMinHeap) {
			while(getParent(position) != 0 && heapArray[getParent(position)].priority.compareTo(heapArray[position].priority) > 0) {
				swap(position, getParent(position));
				position = getParent(position);
			}
		} else {
			while(getParent(position) != 0 && heapArray[getParent(position)].priority.compareTo(heapArray[position].priority) < 0) {
				swap(position, getParent(position));
				position = getParent(position);
			}
		}
	}
	
	private void swap(int position, int parent) {
		HeapElement<T,E> temp = heapArray[position];
		heapArray[position] = heapArray[parent];
		heapArray[parent] = temp;
	}

	private int getParent(int position) {
		return position/2;
	}
	
	private int getLeftChild(int position) {
		return 2*position;
	}
	
	private int getRightChild(int position) {
		return 2*position + 1;
	}
	
	private void sink(int position) {
		while(!isLeaf(position)) {
			int child = getCandidateChild(position);
			if(isMinHeap) {
				if(heapArray[child].priority.compareTo(heapArray[position].priority) < 0) {
					swap(child, position);
					position = child;
				} else {
					break;
				}
			} else {
				if(heapArray[child].priority.compareTo(heapArray[position].priority) > 0) {
					swap(child, position);
					position = child;
				} else {
					break;
				}
			}
		}
		
	}
	
	private int getCandidateChild(int position) {
		int rightChild = getRightChild(position);
		int leftChild = getLeftChild(position);
		if(rightChild > heapSize && leftChild > heapSize) return -1;
		if(rightChild > heapSize) return leftChild;
		
		if(isMinHeap) {
			if(heapArray[leftChild].priority.compareTo(heapArray[rightChild].priority) < 0) {
				return leftChild;
			} else {
				return rightChild;
			}
		} else {
			if(heapArray[leftChild].priority.compareTo(heapArray[rightChild].priority) > 0) {
				return leftChild;
			} else {
				return rightChild;
			}
		}
	}

	private boolean isLeaf(int position) {
		return getLeftChild(position) > heapSize;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for(int i = 1; i<= heapSize; i++) {
			buffer.append(heapArray[i].toString());
		}
		return buffer.toString();
	}

	private class HeapElement<A, B> {
		E priority;
		T data;
		public HeapElement(E priority, T data) {
			this.priority = priority;
			this.data = data;
		}
		
		@Override
		public String toString() {
			return "HeapElement (data = " + data + ", priority = " + priority + ")";
		}
	}
	
	public static void main(String[] args) {
		MyHeap<Integer, Integer> heap = new MyHeap<Integer, Integer>(10, true);
		heap.addToHeap(6, 600);
		heap.addToHeap(1, 100);
		heap.addToHeap(3, 300);
		heap.addToHeap(2, 200);
		heap.addToHeap(5, 500);
		heap.addToHeap(4, 400);
		System.out.println(heap);
		
		System.out.println(heap.removeElement());
		System.out.println(heap);
		heap.addToHeap(1, 100);
		System.out.println(heap);
		
	}
}

